#include <iostream>
#include <math.h>

using namespace std;

int main()
{
	float celsius, fahrenheit, length, breadth, area, radius,volumne;
	float pi = 3.14159,side1, side2, side3, height,s;
	int choice;
	do {
		cout << "\n\n------------WELCOME T0 OUR PROGRAM------------" << endl;

		cout << "1.Convert from Celsius to Fahrenheit" << endl;
		cout << "2.Convert from Fahrenheit to Celsius" << endl;
		cout << "3.Calculate Circumference of a circle" << endl;
		cout << "4.Calculate Area of a circle" << endl;
		cout << "5.Area of Rectangle" << endl;
		cout << "6.Area of Triangle (Heron's Formula)" << endl;
		cout << "7.Volume of Cylinder" << endl;
		cout << "8.Volume of Cone" << endl;
		cout << "9.Quit this program: " << endl;
		cout << "\n\nPlease choose your option: ";
		cin >> choice;
		cout << "\n\n" << endl;

		if (choice == 1)
		{
			cout << "Enter Value of Celsius: ";
			cin >> celsius;

			fahrenheit = (1.8 * celsius) + 32;

			cout << "\nTemperature in Fahrenheit: " << fahrenheit << endl;

		}
		else if (choice == 2) {
			cout << "Enter Value of Fahrenheit: ";
			cin >> fahrenheit;
			celsius = (fahrenheit - 32) * .5556;

			cout << "\nTemperature in Celsius is: " << celsius << endl;
		}
		else if (choice == 3) {
			cout << "Enter radius of circle : ";
			cin >> radius;
			while (radius <= 0) {
				cout << "Number should be positive!" << endl;
				cout << "Enter radius of circle : ";
				cin >> radius;
			}
			float circumference = 2 * pi * radius;
			cout << "Circumference of Circle is: " << circumference << endl;


		}
		else if (choice == 4) {
			cout << "Enter radius of circle : ";
			cin >> radius;
			while (radius <= 0) {
				cout << "Number should be positive!" << endl;
				cout << "Enter radius of circle : ";
				cin >> radius;
			}
			area = pi * (radius * radius);
			cout << "Area of Circle is: " << area << endl;



		}
		else if (choice == 5) {
			cout << "Enter length of rectangle : ";
			cin >> length;
			while (length <= 0) {
				cout << "Number should be positive!" << endl;
				cout << "Enter length of rectangle : ";
				cin >> length;
			}

			cout << "Enter breadth of rectangle : ";
			cin >> breadth;
			while (breadth <= 0) {
				cout << "Number should be positive!" << endl;
				cout << "Enter breadth of rectangle : ";
				cin >> breadth;
			}

			area = length * breadth;
			cout << "Area of rectangle : " << area;

		}
		else if (choice == 6) {
			cout << " Input the length of 1st side  of the triangle : ";
			cin >> side1;
			while (side1 <= 0) {
				cout << "Number should be positive!" << endl;
				cout << "Input the length of 1st side  of the triangle : ";
				cin >> side1;
			}
			cout << " Input the length of 2nd side  of the triangle : ";
			cin >> side2;
			while (side2 <= 0) {
				cout << "Number should be positive!" << endl;
				cout << "Input the length of 2nd side  of the triangle : ";
				cin >> side2;
			}
			cout << " Input the length of 3rd side  of the triangle : ";
			cin >> side3;
			while (side3 <= 0) {
				cout << "Number should be positive!" << endl;
				cout << "Input the length of 3rd side  of the triangle : ";
				cin >> side3;
			}
			s = (double)(side1 + side2 + side3) / 2;
			area = sqrt(s * ((s - side1)*(s - side2)*(s - side3)));
			cout << " The area of the triangle is : " << area << endl;
			cout << endl;
		}
		else if (choice == 7) {
			cout << "Input the radius of the cylinder : ";
			cin >> radius;
			while (radius <= 0) {
				cout << "Number should be positive!" << endl;
				cout << "Input the radius of the cylinder : ";
				cin >> radius;
			}
			cout << "Input the height of the cylinder : ";
			cin >> height;
			while (height <= 0) {
				cout << "Number should be positive!" << endl;
				cout << "Input the height of the cylinder : ";
				cin >> height;
			}
			volumne = (3.14 * (radius * radius) * height);
			cout << "The volume of a cylinder is : " << volumne << endl;
			cout << endl;
		}
		else if (choice == 8) {
			cout << "Input cone's radius: ";
			cin >> radius;
			while (radius <= 0) {
				cout << "Number should be positive!" << endl;
				cout << "Input cone's radius: ";
				cin >> radius;
			}

			cout << "Input cone's height: ";
			cin >> height;
			while (height <= 0) {
				cout << "Number should be positive!" << endl;
				cout << "Input cone's height: ";
				cin >> height;
			}

			volumne = (1.0 / 3.0) * pi * (radius * radius) * height;

			cout << "The volume of the cone is: " << volumne;
		}


	} while (choice != 9);

	cout << "\n\nQuiting" << endl;
	cout << "Thank you" << endl;
	cout << "Byee" << endl;

	return 0;
}
